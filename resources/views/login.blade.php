<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Inicio de Sesión</title>

<!-- CSS Stylesheets-->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/tether.min.css" />
<link href="css/sweetalert2.css" rel="stylesheet" />



</head>
<body>


<div class="container">
	
	<div class="jumbotron loginbox ">
		<div class="img-login img-responsive text-center">
			<img src="images/login.png" alt="Iniciar Sesion" class="img-login">
			<h2>Iniciar Sesion </h2>
		</div>
		<div id="loginform">

		<form action="dashboard" >
		<meta name="_token" content="{!! csrf_token() !!}"/>
			<label for="username">Usuario: </label>
			<input type="text" v-model="Username" placeholder="Usuario" name="usuario" id="usuario" class="form-control">

			
			<label for="password">Contraseña: </label>
			<input v-model="Password" type="password" name="password" id="password" placeholder="*********" class="form-control">
			<div class="text-center "><br>
			<input v-on:click="logear" type="button" value="Iniciar Sesión" class="btn btn-success btn-lg">
			</div>
		</form>
	</div>
	</div>
</div><!-- divfinal-->



<!-- JS Stylesheets-->
@include('layouts.footer')
<script  type="text/javascript" src="js/form/login.js"></script>
</body>
</html>