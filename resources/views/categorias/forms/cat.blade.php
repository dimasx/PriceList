

<div class="form-group">
	{!! Form::label('Nombre') !!} 
	{!! Form::text('nombre',null,['id'=>'nombre_cat','class'=>'form-control','placeholder'=>'Material']) !!}
</div>
<div class="form-group">
	{!! Form::label('Descripción') !!} 
	{!! Form::text('descripcion',null,['id'=>'desc_cat','class'=>'form-control','placeholder'=>'Material para construccion']) !!}
</div>
<div class="form-group">
	{!! Form::label('Status') !!}
	{!! Form::select('rol', array('1' => 'Activo', '0' => 'Desabilitado'), ['id'=>'status_cat','class'=>'form-control']) !!}
</div>
         