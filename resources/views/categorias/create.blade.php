@extends('layouts.principal')

@section('content')
@include('alerts/request')
<input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
{!! Form::open(['route'=>'categorias.create', 'method'=>'POST'])!!}
@include('categorias.forms.cat')
{!!link_to('#', $title='registrar',$attributes=['id'=>'registro', 'class'=>'btn btn-primary', $secure = null])!!}

{!! Form::close() !!}

</div>
@stop