@include('layouts.principal')
<div id="modedit">
<meta name="_token" content="{!! csrf_token() !!}"/>
          <h1 class="page-header">Gestión de Categorías</h1>



      
          <div class="col-md-6">
            <div class="form-group">
            <label for="Nombre">Nombre: </label>
            <input v-model="fd.Name" type="text" class="form-control" id="nombre_categoria"
                   placeholder="Introduce la Categoria" name="nombre_categoria">
          </div>
          </div>
          <div class="col-md-6">
             <div class="form-group">
            <label for="ejemplo_password_1">Descripción</label>
            <input v-model="fd.Description" type="text" name="descripcion" class="form-control" id="Descripción" 
                   placeholder="Descripción">
          </div>
          </div>
          
         
          <div class="row">
          <button v-on:click="guardar" type="submit" class="btn btn-success">Guardar</button>
          <button v-on:click="editar" type="submit" class="btn btn-warning">Actualizar</button>
          <button v-on:click="borrar" type="submit" class="btn btn-danger">Eliminar</button>
       </div>
   
   </div>   
<br><br>
<br>
<div class="panel panel-primary">
    <!-- <h3 class="titleform">Requerimientos de Sistema</h3>-->
    <div class="panel-heading panel-heading-orange">
        <h3 class="panel-title">Listado de Categorias</h3>
    </div>
<div class="panel-body">
      
      <br>
      <div id="actiontable">
      
       </div>
      <!-- ====== DATATABLE ======= -->
      <table id="CategorieTable" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Idn</th>
            <th>Nombre</th>
            <th>Descripción</th>
        
          </tr>

        </thead>

      </table>

      <!-- ====== FIN DATATABLE======= -->
    </div>
     </div>
    </div>
    @include('layouts.footer')
    <script src="js/form/categorie.js"></script>