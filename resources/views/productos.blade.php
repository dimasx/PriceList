@include('layouts.principal')

<div id="modedit">
<meta name="_token" content="{!! csrf_token() !!}"/>
<div class="row">

  </div>
          <h1 class="page-header">Gestión de Productos</h1>
<div class="row">
          <div class="col-md-6">
             
             <div class="form-group">
              <label  for="Cod">Código: </label>
              <input v-model="fd.Cod" type="text" class="form-control" placeholder="Introduce el Código">
            </div>
            <div class="form-group">
              <label  for="Nombre">Nombre: </label>
              <input v-model="fd.Name" type="text" class="form-control" placeholder="Introduce el nombre">
            </div>
            <div class="form-group">
              <label for="ejemplo_password_1">Descripción</label>
              <input v-model="fd.Description" type="text" name="descripcion" class="form-control" id="Descripción" 
                     placeholder="Descripción">
            </div>
              <div class="form-group">
                <label for="ejemplo_password_1">Categoria</label>
                       
                            <select v-model="fd.Categorie" class="form-control">
                             <option value="">Seleccione una</option>
                             <option v-for="Categorie in fd.Categorieoptions" v-bind:value="Categorie.idn">
                              @{{ Categorie.name }}
                            </option>
                          </select>
              </div>

          </div>


          <div class="col-md-6">
              <div class="form-group">
            <label for="ejemplo_password_1">Costo</label>
                <div class="row">
                    <div class="col-md-6">
                      <input v-model="fd.Cost" type="text" name="Cost" class="col-sm-6 form-control" id="Cost" placeholder="Indica el costo">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>            
              </div>
             <div class="form-group">
            <label for="ejemplo_password_1">Precio 1</label>
                <div class="row">
                    <div class="col-md-6">
                      <input v-model="fd.Price1" type="text" name="precio1" class="col-sm-6 form-control" id="precio1" placeholder="Indica un Precio">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>            
              </div>

         <div class="form-group">
            <label for="ejemplo_password_1">Precio 2</label>
                <div class="row">
                    <div class="col-md-6">
                      <input v-model="fd.Price2" type="text" name="precio2" class="col-sm-6 form-control" id="precio1" placeholder="Indica un Precio">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>            
              </div>


          <div class="form-group">
            <label for="ejemplo_password_1">Precio 3</label>
                <div class="row">
                    <div class="col-md-6">
                      <input v-model="fd.Price3" type="text" name="precio3" class="col-sm-6 form-control" id="precio1" placeholder="Indica un Precio">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>            
              </div>

          </div>

     </div> 
         
       <div class="row">
          <button v-on:click="guardar" type="submit" class="btn btn-success">Guardar</button>
          <button v-on:click="editar" type="submit" class="btn btn-warning">Actualizar</button>
          <button v-on:click="borrar" type="submit" class="btn btn-danger">Eliminar</button>
       </div>
      
<br><br>
<br>
</div>
<div class="panel panel-primary">
    <!-- <h3 class="titleform">Requerimientos de Sistema</h3>-->
    <div class="panel-heading panel-heading-orange">
        <h3 class="panel-title">Listado de Productos</h3>
    </div>
   <div class="panel-body">
      
      <br>
      <div id="actiontable">
      
       </div>
      <!-- ====== DATATABLE ======= -->
      <table id="ProductTable" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Idn</th>
            <th>Cod</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Categoria</th>
            <th>Precio 1</th>
            <th>Precio 2</th>
            <th>Precio 3</th>
          </tr>

        </thead>

      </table>

      <!-- ====== FIN DATATABLE======= -->
    </div>
  </div>
          

        </div>
      </div>
</div>
@include('layouts.footer')
<script src="js/form/product.js"></script>