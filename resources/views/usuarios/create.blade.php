@extends('layouts.principal')

@section('content')
@include('alerts/request')

{!! Form::open(['route'=>'usuario.store', 'method'=>'POST'])!!}
@include('usuarios.forms.usr')

{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}

</div>
@stop