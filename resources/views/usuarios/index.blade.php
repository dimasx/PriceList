@extends('layouts.principal')

<?php $message=Session::get('message') ?>

@section('content')
@if(Session::has('message'))
	<div class="alert alert-success">
  	<strong>Success!</strong> 
  	{{ Session::get('message') }}
	</div>
@endif

<table class="table">
	<thead>
		<th>Usuario</th>
		<th>Nombres</th>
		<th>Apellidos</th>
		<th>Correo</th>
		<th>Privilegios</th>
		<th>Editar</th>
	</thead>
	@foreach($users as $user)
	<tbody>
		<th>{{ $user->usuario }}</th>
		<th>{{ $user->nombres }}</th>
		<th>{{ $user->apellidos }}</th>
		<th>{{ $user->email }}</th>
		<th>{{ $user->rol }}</th>
		<th>{!! link_to_route('usuario.edit', $title = 'editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary']) !!}</th>
		
	</tbody>
	@endforeach
</table>

@stop