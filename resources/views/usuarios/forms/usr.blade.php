<div class="form-group">
	{!! Form::label('Nombres') !!}
	{!! Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese los Nombres']) !!}
</div>
<div class="form-group">
	{!! Form::label('Apellidos') !!}
	{!! Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese los Apellidos']) !!}
</div>
<div class="form-group">
	{!! Form::label('Usuario') !!}
	{!! Form::text('usuario',null,['class'=>'form-control','placeholder'=>'Ingrese su Nombre de Usuario']) !!}
</div>
<div class="form-group">
	{!! Form::label('Correo') !!} 
	{!! Form::text('email',null,['class'=>'form-control','placeholder'=>'example@gmail.com']) !!}
</div>
<div class="form-group">
	{!! Form::label('Clave') !!}
	{!! Form::password('password',['class'=>'form-control','placeholder'=>'Ingrese la Clave']) !!}
</div>
<div class="form-group">
	{!! Form::label('Privilegio') !!}
	{!! Form::select('rol', array('0' => 'Admin', '1' => 'Usuario'), ['class'=>'form-control']) !!}
</div>