<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Pricelist</title>

  <!-- CSS Stylesheets-->
  {!! Html::style('css/bootstrap.min.css') !!}
  {!! Html::style('css/style.css') !!}
  {!! Html::style('css/tether.min.css') !!}



  <!--     Fonts and icons     -->
  <link href="css/font-awesome.min.css" rel="stylesheet">

  <link href='css/fonts.googleapis.css' rel='stylesheet' type='text/css'>
 

  <link rel="stylesheet" href="css/jquery.dataTables.min.css">
  <!-- Bootstrap core CSS     -->
  <link href="css/sweetalert2.css" rel="stylesheet" />
 <meta name="_token" content="{!! csrf_token() !!}"/>
</head>
<body>


  <div class="container">
    <nav  class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Listado de Precios</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"></a></li>
            <li><button class="btn btn-primary" v-on:click="close">Salir</button></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="{!!URL::to('/dashboard')!!}" class="">Inicio<span class="sr-only ">(current)</span></a></li>
            <li><a href="{!!URL::to('/productos')!!}" class="">Gestión de Productos</a>
            <li><a href="{!!URL::to('/categorias')!!} " class="">GestiónCategorias</a></li>
            <!--<li><a href="{!!URL::to('/usuarios')!!}" class="">Usuarios</a>-->


            </li>
            <li>


            </div>




          </li>
        </ul>
      </div><br>
      <div class="col-md-10 col-md-offset-2 main"><br>

       
