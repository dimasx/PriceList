<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//RUTAS PARA ACCEDER A VENTANAS FRONTEND
Route::get('dashboard', 'FrontController@dashboard');

Route::get('categorias', 'FrontController@categorias');
Route::get('productos', 'FrontController@productos');

//Normalmente las api se version con v1, para luego poder tener v2 y nuevas versiones.
//Tambien es recomendable usar las rutas en ingles, las del frontend si pueden ir en español
Route::resource('api/v1/user', 'UserController');
Route::resource('api/v1/categorie', 'CategorieController');
Route::resource('api/v1/product', 'ProductController');
Route::post('api/v1/user/login','UserController@login');
Route::post('api/v1/user/closesession','UserController@closeLoginSession');

Route::put('api/v1/user/updatepass/{idn}','UserController@updatepassword');
//RUTAS DE SERVICIOS DE BACKEND



//RUTA AL INICIAR DEBERIA SER EL LOGIN
Route::get('login', 'FrontController@login');
Route::get('/', 'FrontController@login');