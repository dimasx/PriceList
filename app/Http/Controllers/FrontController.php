<?php namespace PriceList\Http\Controllers;

use PriceList\Http\Requests;
use PriceList\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FrontController extends Controller {


	public function __construct(){
	
	
	$this->middleware('validateroot', ['only' =>
	 ['dashboard',
	 'productos',
	 'categorias'
	]]);



    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function dashboard()
	{
		return view('dashboard');
	}
	public function login()
	{
		return view('login');
	}
	public function productos()
	{
		return view('productos');
	}
	public function categorias()
	{
		return view('categorias');
	}




}
