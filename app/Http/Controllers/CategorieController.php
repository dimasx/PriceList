<?php namespace PriceList\Http\Controllers;

use PriceList\Http\Requests;
use PriceList\Http\Controllers\Controller;
use PriceList\Categorie;
use Hash;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class CategorieController extends Controller {

 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Table::select('name','surname')->where('id', 1)->get();
         $Categories = Categorie::select('idn','name','description','active')->where('active', 1)->get();

        return \Response::json($Categories,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*  Categorie::create([
            'nombre' => $request['nombre']
            
        ]);*/
        $Name = $request['Name'];
         $Description = $request['Description'];
        if ($Name == "")
        {
              return \Response::json("El Nombre no puede estar en nulo",500);
        }
       
     try
        {
            $Categorie = new Categorie;
            $Categorie->name = $Name;
            $Categorie->description = $Description;
            $Categorie->save();
        return \Response::json('Guardado correctamente: '.$Name,200);

         }
          //CATCH COMPROBACION
          catch (QueryException $e)
          {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if($errorCode == 1062){
                return \Response::json("Ya existe un elemento igual",409);
            }
            else
            {
                return \Response::json("ERROR AL OBTENER LOS DATOS",500);
            }
            

          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idn)
    {
         $Name = $request['Name'];
         $Description = $request['Description'];
        if ($Name == "")
        {
              return \Response::json("El Nombre no puede estar en nulo",500);
        }
        try
        {

            $Categorie = Categorie::find($idn);

            $Categorie->name = $Name;
            $Categorie->description = $Description;
            $Categorie->save();
        return \Response::json('Modificado correctamente: '.$Name,200);

         }
          //CATCH COMPROBACION
          catch(PDOException $e)
          {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try
        {

            $Categorie = Categorie::find($id);
            $Categorie->active = 0;
            $Categorie->save();
        return \Response::json('Eliminado correctamente: ',200);

         }
          //CATCH COMPROBACION
          catch(PDOException $e)
          {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

          }
    }
}
