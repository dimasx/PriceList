<?php namespace PriceList\Http\Controllers;

use PriceList\Http\Requests;
use PriceList\Http\Requests\UserCreateRequest;
use PriceList\Http\Requests\UserUpdateRequest;
use PriceList\Http\Controllers\Controller;
use PriceList\User;
use Hash;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function index()
    {
     //MUESTRA TODOS LOS USUARIOS ACTIVOS=1

          $users = DB::table('user')            
            ->select('idn','username','idnrol','active')
            ->where('active',1)
            ->get();

        return \Response::json($users,200);
    }
     public function login(Request $request)
    {
    	//LOGEO DE USUARIO; RECIBO LOS 2 parametros
        $Username = $request['username'];
        $Password = $request['password'];
          if ($Username == "")
        {
              return \Response::json("El Username no puede estar en nulo",500);
        }

        $user = User::select('idn','username','idnrol','password','active')
        ->where('active', 1)
        ->where('username', $Username)        
        ->get()->first();
        if($user === null)
        {
            return \Response::json("CREDENCIALES INCORRECTAS",404); 
        }
        //CHECKING PASSWORD
        if (Hash::check($Password, $user->password))
        {
           $Idn = $user->idn;
           $IdnRol = $user->idnrol;
           $Username = $user->username;
          

           session()->set('IdnUsuario', $Idn);
           session()->set('IdnRolUsuario', $IdnRol);
           session()->set('UsernameUsuario', $Username);


            return \Response::json(["BIENVENIDO",$Idn,$IdnRol,$Username,200]);
        }
        else
        {

        return \Response::json("CREDENCIALES INCORRECTAS",404); 
        }


    }
    public function closeLoginSession()
    {
        //DATOS QUE VIENEN DESDE EL POST AJAX
        session()->set('IdnUsuario', '');
        session()->set('IdnRolUsuario', '');
        session()->set('UsernameUsuario', '');


        return \Response::json(["SESION CERRADA"],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //CACH DATA
        $Idnrol = $request['idnrol'];
        $Username = $request['username'];
        $Password = $request['password'];
        if ($Username == "")
        {
              return \Response::json("El Username no puede estar en nulo",500);
        }
       
     try
        {
            $user = new User;
            $user->username = $Username;
            $user->idnrol = $Idnrol;
            $user->password = bcrypt($Password);
            $user->save();
        return \Response::json('Guardado correctamente: '.$Username,200);

         }
          //CATCH COMPROBACION
          catch (QueryException $e)
          {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if($errorCode == 1062){
                return \Response::json("Ya existe un elemento igual",409);
            }
            else
            {
                return \Response::json("ERROR AL OBTENER LOS DATOS",500);
            }
            

          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idn)
    {
         //CACH DATA
        $Idnrol = $request['idnrol'];
        $Username = $request['username'];
        
        if ($Username == "")
        {
              return \Response::json("El Username no puede estar en nulo",500);
        }
        try
        {

            $user = User::find($idn);
            $user->username = $Username;
            $user->idnrol = $Idnrol;
            $user->save();
        return \Response::json('Modificado correctamente: '.$Username,200);

         }
          //CATCH COMPROBACION
          catch(PDOException $e)
          {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

          }
    }
  public function updatepassword(Request $request, $idn)
    {
         //CACH DATA
       
        $Username = $request['username'];
        $Password = $request['password'];
        
        if ($Username == "")
        {
              return \Response::json("El Username no puede estar en nulo",500);
        }
        try
        {

            $user = User::find($idn);
            $user->password = bcrypt($Password);           
            $user->save();
        return \Response::json('Modificado correctamente: '.$Username,200);

         }
          //CATCH COMPROBACION
          catch(PDOException $e)
          {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

          }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        try
        {

            $user = User::find($id);
            $user->active = 0;
            $user->save();
        return \Response::json('Eliminado correctamente: ',200);

         }
          //CATCH COMPROBACION
          catch(PDOException $e)
          {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

          }
    }

}
