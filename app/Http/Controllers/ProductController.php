<?php namespace PriceList\Http\Controllers;

use PriceList\Http\Requests;
use PriceList\Http\Controllers\Controller;
use PriceList\Product;
use Hash;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 //MUESTRA TODOS LOS PRODUCTOS ACTIVOS=1

          $users = DB::table('product')
          //Incluye un inner join para ver la categoria a la que pertenece
            ->join('categorie', 'categorie.idn', '=', 'idncategorie')
            ->select('product.idn','product.cod','product.name','product.description','product.idncategorie','categorie.name as namecategorie','product.cost','product.price1','product.price2','product.price3','product.active')
            ->where('product.active',1)
            ->get();
        return \Response::json($users,200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return "Crear Productos";
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		  //CACH DATA
        $cod = $request['Cod'];
        $name = $request['Name'];
        $description = $request['Description'];
        if ($cod == "")
        {
              return \Response::json("el codigo no puede ser nulo",500);
        }

       	$ProductFinded = Product::select('cod')->where('cod', $cod)->get();

       	if($ProductFinded->count()>0)
       	{
       		 return \Response::json("Este Código ya existe",309);
       	}
        $idncategorie = $request['Categorie'];
        $cost = $request['Cost'];
        $price1 = $request['Price1'];
        $price2 = $request['Price2'];
        $price3 = $request['Price3'];       
     try
        {
            $product = new Product;
            $product->cod = $cod;
          	$product->name = $name;
          	$product->description = $description;
          	$product->idncategorie = $idncategorie;
            $product->cost = $cost;
          	$product->price1 = $price1;
          	$product->price2 = $price2;
          	$product->price3 = $price3;
          

            $product->save();
        return \Response::json('Guardado correctamente: '.$product,200);

         }
          //CATCH COMPROBACION
          catch (QueryException $e)
          {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if($errorCode == 1062){
                return \Response::json("Ya existe un elemento igual",409);
            }
            else
            {
                return \Response::json("ERROR AL OBTENER LOS DATOS",500);
            }
            

          }
    }
	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$cod = $request['Cod'];
        $name = $request['Name'];
        $description = $request['Description'];
        if ($cod == "")
        {
              return \Response::json("el codigo no puede ser nulo",500);
        }

       	$ProductFinded = Product::select('cod')
       	->where('cod', $cod)
       	->where('idn', '!=',$id)
       	->get();

       	if($ProductFinded->count()>0)
       	{
       		 return \Response::json("Este Código ya existe",309);
       	}
        $idncategorie = $request['Categorie'];
        $cost = $request['Cost'];
        $price1 = $request['Price1'];
        $price2 = $request['Price2'];
        $price3 = $request['Price3'];      

		 try
        {
            $product = Product::find($id);
            $product->cod = $cod;
          	$product->name = $name;
          	$product->description = $description;
          	$product->idncategorie = $idncategorie;
            $product->cost = $cost;
          	$product->price1 = $price1;
          	$product->price2 = $price2;
          	$product->price3 = $price3;
            $product->save();


            return \Response::json(["EDITADO CORRECTAMENTE",$product],200);

        }
            //CATCH COMPROBACION
        catch (QueryException $e)
        {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if($errorCode == 1062){
                return \Response::json("Ya existe un elemento igual",409);
            }
            else
            {
                return \Response::json("ERROR AL OBTENER LOS DATOS",500);
            }


        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	 {
        try
        {

            $Product = Product::find($id);
            $Product->active = 0;
            $Product->save();
            return \Response::json('Eliminado correctamente: ',200);

        }
            //CATCH COMPROBACION
        catch(PDOException $e)
        {
            return \Response::json("ERROR AL OBTENER LOS DATOS",500);

        }
    }

}
