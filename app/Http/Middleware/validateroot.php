<?php namespace PriceList\Http\Middleware;

use Closure;

class validateroot {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		
		$IdnRol = session()->get('IdnRolUsuario');

		if($IdnRol == "")
		{
			return redirect('/');
        }
        else
        {
         return $next($request);

            
        }
     

	}

}
