<?php

namespace PriceList\Http\Requests;

use PriceList\Http\Requests\Request;

class UserCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'nombres'=>'required',
        'apellidos'=>'required',
        'password'=>'required',
        'email'=>'required|unique:users',
        'usuario'=>'required|unique:users',
        'rol'=>'required',
            //
        ];
    }
}
