<?php

namespace PriceList;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';
    protected $primaryKey = 'idn';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   // protected $fillable = ['username','password','idnrol'];
}
