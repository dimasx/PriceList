Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');
function sendData(url,method,arr)
{
  //FUNCTION RECIBE: URL A DONDE ENVIAR; METODO (SAVE O UPDATE), ARR CON DATOS
  var action = '';
  //Si la funcion es guardar
if (method ==="save")
  {
    swal({
      title: "Confirmación de Guardado",
      text: "Estas seguro de guardar este elemento",
      type: "warning",
      showCancelButton: true,       
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(function () {     
            //Envio el json por POST a la URL especificada
             Vue.http.post('api/v1/'+url,arr).then(response => {
              //Si todo funciona envio mensaje de confirmacion
              // get status
              var code = response.status;
              if (code === 200)
              {
                swal('Guardado Correctamente','','success');              
                 mod.refreshdata();
                 mod.cleanform();
              }
            
              
  
           }).then().catch(function (data){
            if(data.status === 309) {
             swal('El Elemento ya Existe','','warning');
         }
         if(data.status === 404) {
             swal('Data no encontrada','','error');
         }
         if(data.status === 500) {
             swal('Ocurrio un error al procesar','','error');
         }
         });
    })
  }
  else if (method === "update")
    {
      swal({
        title: "Confirmación de modificación",
        text: "Estas seguro de modificar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {     
                //POR DEFECTO ENVIA CON LA URL API/V1
               Vue.http.put('api/v1/'+url,arr).then(response => {
                // success callback
                swal('Modificado Correctamente','','success');

                mod.refreshdata();
                mod.cleanform();
    
                }).then().catch(function (data){
            if(data.status === 309) {
             swal('Un elemento con estos datos ya Existe','','warning');
         }
         if(data.status === 404) {
             swal('Data no encontrada','','error');
         }
         if(data.status === 500) {
             swal('Ocurrio un error al procesar','','error');
         }
         });
      })
    }
    else if (method === "delete")
        {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,       
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
              }).then(function () {
                Vue.http.delete('api/v1/'+url,arr).then(response => {
                    // success callback
                     swal('Eliminado Correctamente','','success');       
                     mod.refreshdata();
                     mod.cleanform();
                  }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                  });
          })  
        }
 
}

//COLORS
var green = '#98FEE6';
var green2 = '#D0FF9A';
var yellow = '#FFF4D7';
var red = '#FFB1B1';
var blue = '#9ADCFF';
var gray = '#b0b0b0';
var lite = '#e2e2e2';

function Pintar(color, column, row)
{ 
    $('td:eq('+column+')', row).css('background-color', color);
}
function CargarTablaRequerimientos(NameTable,datacolumns,Url,indice)
{
var table = $("#"+NameTable+"").DataTable({
  //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
language: {
      url: 'json/Spanish.json'
  }, 
  "rowCallback": function( row, data, index ) {                
    
       //a b c es el identificado de la columna para colorearla
       var a = indice;
       var b = a+1;
       var c = b+1;
       switch(data.priority) {
         case "Baja": Pintar(green,a,row);break;
         case "Normal": Pintar(blue,a,row);break;
         case "Urgente":Pintar(yellow,a,row);break;
         case "Muy Urgente":Pintar(red,a,row);                
     }
     switch(data.statusdeveloper) {
       case "Por Revisar":Pintar(yellow,b,row); break;
       case "En Proceso": Pintar(blue,b,row); break;
       case "Actualizado": Pintar(green,b,row); break;
       case "Dudas": Pintar(green2,b,row); break;                          
   }          
   switch(data.statusanalist) {
     case "Por Verificar":Pintar(yellow,c,row); break;
     case "Fallando": Pintar(red,c,row); break;
     case "Verificado": Pintar(green,c,row); break;                        
 }      
 //Pintar la primera fila con un color por default;   
     Pintar(lite,0,row);
 },


"destroy": true,
"scrollY":        "500px",
"scrollX":        "550px",
"iDisplayLength": 50,
 "fixedColumns": true,
//Especificaciones de las Columnas que vienen y deben mostrarse
"columns" : datacolumns,    
//Especificaciones de la URL del servicio
"ajax": {
url: "api/v1/"+Url+"",
dataSrc : ''
}


});  
return table;
}

function CargarTablaGenerica(NameTable,datacolumns,Url,indice)
{
var table = $("#"+NameTable+"").DataTable({
  //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
language: {
      url: 'json/Spanish.json'
  }, 
  "rowCallback": function( row, data, index ) {               
   
 //Pintar la primera fila con un color por default;   
     Pintar(lite,0,row);
 },


"destroy": true,
"scrollY":        "500px",
"scrollX":        "550px",
"iDisplayLength": 50,
 "fixedColumns": true,
//Especificaciones de las Columnas que vienen y deben mostrarse
"columns" : datacolumns,    
//Especificaciones de la URL del servicio
"ajax": {
url: "api/v1/"+Url+"",
dataSrc : ''
}


});  
return table;
}


      //FUNCIONES EN LA MODAL DE EDICION
      var nv = new Vue({
          el: '#navbar',
          data: {

          },
          methods: {
              close:function()
              {
                  swal({
                      title: "Confirmación de Salida",
                      text: "¿Estas seguro de salir?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Aceptar',
                      cancelButtonText: 'Cancelar'
                  }).then(function () {
                      arr={};

                      Vue.http.post('api/v1/user/closesession',arr).then(response => {
                          // success callback
                         window.location.href="/";

                  }, response => {
                          swal('Ocurrio un error al salir','','error')

                      });
                  })
              },

          }

      });
