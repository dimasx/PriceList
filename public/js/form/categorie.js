

Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods:
        {
            loadtable:function()
            {
                
                LoadTableProducts();
               
                },
          
        }
});

vm.loadtable();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#modedit',
    data: {
        globalurl:'categorie',
        fd:
        {
        Idn:'',
        //DATOS
        Name:'',           
        Description:''
       
        },
        ff:
        {
          disabled:false,
          save:false,
          edit:true 
        }
       
    },
    methods: {
       
      
    guardar: function (event) 
    {
        var arr = { Idn:mod.fd.Idn,
                  Name:mod.fd.Name,
                 
                  Description:mod.fd.Description
                           
                };
      //envio la url, el metodo, y el arreglo de datos.
       sendData(mod.globalurl,'save',arr); 
            
       
    },
    editar: function (event) 
    {
        var arr = { Idn:mod.fd.Idn,
                  Name:mod.fd.Name,
                
                  Description:mod.fd.Description
                  };   
        //envio la url, el metodo, y el arreglo de datos.
       sendData(mod.globalurl+'/'+mod.fd.Idn,'update',arr);  
       //(Process,User,ID) 
    
        
    },  
    borrar: function (event)
    {
       var arr = {name:mod.Name};
             //envio la url, el metodo, y el arreglo de datos.
       var result = sendData(mod.globalurl+'/'+mod.fd.Idn+'','delete',arr);
     
    },
    //Limpia los datos en la pantalla o formulario
    cleanform:function()
    {
        mod.fd.Idn='';
        //DATOS DE FORMULARIO
        mod.fd.Name='';
       
        mod.fd.Description='';       
        
     
    },
    //Refresca la pantalla actualizando los datos en la tabla
    refreshdata:function()
    {  
       var table = $('#ProductTable').dataTable(); 
     table.fnReloadAjax();     
    }

    }

});
//======INICIO DE FUNCIONES ============
function LoadTableProducts()
{


    var table = $("#CategorieTable").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "500px",
        "scrollX":        "700px",

        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },          
            { data : 'name'},
            { data : 'description'}         
          
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: "api/v1/categorie",
            dataSrc : ''
        }

    });

 $('#CategorieTable tbody').on( 'click', 'tr', function () {
      
        $("html, body").animate({ scrollTop: 0 }, "slow");
        mod.fd.Idn = table.row( this ).data().idn;       
        mod.fd.Name = table.row( this ).data().name;
        
        mod.fd.Description = table.row( this ).data().description;
      
           
      //ABRO LA MODAL
      

 });

}
