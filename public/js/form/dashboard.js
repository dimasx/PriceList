


//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods:
        {
            loadtable:function()
            {
                
                LoadTableProducts();
               
                }
        }
});

vm.loadtable();


//======INICIO DE FUNCIONES ============
function LoadTableProducts()
{


    var table = $("#ProductTable").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "500px",
        "scrollX":        "700px",

        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'cod'},
            { data : 'name'},           
            { data : 'namecategorie'},          
            { data : 'price1'},
            { data : 'price2'},
            { data : 'price3'} 
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: "api/v1/product",
            dataSrc : ''
        }

    });



}





