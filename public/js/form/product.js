

Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods:
        {
            loadtable:function()
            {
                
                LoadTableProducts();
               
                },
            loadcombocategories:function()
                {
                           // GET /someUrl
                    this.$http.get('api/v1/categorie').then(response => {

                    // get body data
                    mod.fd.Categorieoptions = response.body;

                  }, response => {
                    // error callback
                  });
                }
        }
});

vm.loadtable();
vm.loadcombocategories();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#modedit',
    data: {
        globalurl:'product',
        fd:
        {
        Idn:'',
        //DATOS
        Name:'',
        Cod:'',     
        Description:'',
        Categorie:'',
        Categorieoptions:'',
        Cost:'',        
        Price1:'',
        Price2: '',
        Price3: '',
        Userloged:$("#UsernameUsuario").text(),
        disabled:true,
        save:false,
        edit:true
        },
        ff:
        {
          disabled:false,
          save:false,
          edit:true 
        }
       
    },
    methods: {
       
      
    guardar: function (event) 
    {
        var arr = { Idn:mod.fd.Idn,
                  Name:mod.fd.Name,
                  Cod:mod.fd.Cod,
                  Description:mod.fd.Description,
                  Categorie:mod.fd.Categorie,
                  Cost:mod.fd.Cost,
                  Price1:mod.fd.Price1,                  
                  Price2:mod.fd.Price2,
                  Price3:mod.fd.Price3                
                };
      //envio la url, el metodo, y el arreglo de datos.
       sendData(mod.globalurl,'save',arr); 
            
       
    },
    editar: function (event) 
    {
        var arr = { Idn:mod.fd.Idn,
                  Name:mod.fd.Name,
                  Cod:mod.fd.Cod,
                  Description:mod.fd.Description,
                  Categorie:mod.fd.Categorie,
                  Cost:mod.fd.Cost,
                  Price1:mod.fd.Price1,                  
                  Price2:mod.fd.Price2,
                  Price3:mod.fd.Price3  };   
        //envio la url, el metodo, y el arreglo de datos.
       sendData(mod.globalurl+'/'+mod.fd.Idn,'update',arr);  
       //(Process,User,ID) 
    
        
    },  
    borrar: function (event)
    {
       var arr = {name:mod.Name};
             //envio la url, el metodo, y el arreglo de datos.
       var result = sendData(mod.globalurl+'/'+mod.fd.Idn+'','delete',arr);
     
    },
    //Limpia los datos en la pantalla o formulario
    cleanform:function()
    {
        mod.fd.Idn='';
        //DATOS DE FORMULARIO
        mod.fd.Name='';
        mod.fd.Cod='';    
        mod.fd.Description='';
        mod.fd.Cost='';        
        mod.fd.Price1='';
        mod.fd.Price2='';
        mod.fd.Price3='';
        
     
    },
    //Refresca la pantalla actualizando los datos en la tabla
    refreshdata:function()
    {  
       var table = $('#ProductTable').dataTable(); 
     table.fnReloadAjax();     
    }

    }

});
//======INICIO DE FUNCIONES ============
function LoadTableProducts()
{


    var table = $("#ProductTable").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "500px",
        "scrollX":        "700px",

        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'cod'},
            { data : 'name'},
            { data : 'description'},           
            { data : 'namecategorie'},          
            { data : 'price1'},
            { data : 'price2'},
            { data : 'price3'} 
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: "api/v1/product",
            dataSrc : ''
        }

    });

 $('#ProductTable tbody').on( 'click', 'tr', function () {
      
        $("html, body").animate({ scrollTop: 0 }, "slow");
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Cod = table.row( this ).data().cod;
        mod.fd.Name = table.row( this ).data().name;
        
        mod.fd.Description = table.row( this ).data().description;
        mod.fd.Categorie = table.row( this ).data().idncategorie;
         mod.fd.Cost = table.row( this ).data().cost;
        mod.fd.Price1 = table.row( this ).data().price1;
        mod.fd.Price2 = table.row( this ).data().price2;
        mod.fd.Price3 = table.row( this ).data().price3;
           
      //ABRO LA MODAL
      

 });

}
