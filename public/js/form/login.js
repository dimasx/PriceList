



//FUNCIONES EN LA MODAL DE EDICION
var login = new Vue({
  el: '#loginform',
  data: {    
    Username:'',
    Password:''
   
  },
  methods: {
      keymonitor: function(event) {

          if (event.key == "Enter") {
              this.logear(event);
          }
      },
    logear: function (event) 
    {
       
     if (login.Username == '' || login.Password == '')
     {
         swal('Por favor, completa los campos para acceder','','warning');
     }
     else
     {
         var arr = {username:login.Username,password:login.Password};
         //Token de seguridad que viene desde la ventana, asi se evita que hagan post desde otros lugares
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=_token]').attr('content');
		//Envio de datos por post ajax
        Vue.http.post('api/v1/user/login',arr)
             .then(response => {

             if (response.status === 200)
         {
             // success callback
             swal({
                 title: "Bienvenido",
                 text:'Accediendo',
                 type: "success"
             }).then(function ()
             {
                 window.location.href = "dashboard";
             });

         }

     }).then().catch(function (data){
         if(data.status === 404) {
             swal('Credenciales Incorrectas','','error');
         }
         if(data.status === 500) {
             swal('Ocurrio un error interno','','error');
         }
     });
     }

      
    }
  }
 
});

 




