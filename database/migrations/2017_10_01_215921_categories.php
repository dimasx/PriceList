<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    
     Schema::create('categorie', function (Blueprint $table) {
            $table->increments('idn'); //IDN autoincrementable
            //Indico que el codigo sea unico
            $table->string('name');
            $table->string('description');           
            //Para la eliminacion Logica                           
            $table->integer('active')->default(1); //Si no lo indico por default insertara 1.
            //Crea los atributos Create at y modified at          
            $table->timestamps();
        });
                   //Con esto inserto un articulo por default al correr la migracion
            DB::table('categorie')
                        ->insert(array(                               
                                'name'=>'Modulos', 
                                'description' => 'Categoria de Modulos'                                                        
                               ));

                     

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Esta funcion permite eliminar la tabla al ahcer roolback (ir hacia atras)
        Schema::drop('categorie');
    }
}
