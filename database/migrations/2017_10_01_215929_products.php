<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    
     Schema::create('product', function (Blueprint $table) {
            $table->increments('idn');
            //Indico que el codigo sea unico
            $table->string('cod')->unique();          
            $table->string('name');
            $table->string('description');
            //Solo un Idn sin claves foraneas
            $table->integer('idncategorie'); 
            $table->float('cost');  
            $table->float('price1');  
            $table->float('price2');       
            $table->float('price3'); 
            //Para la eliminacion Logica                           
            $table->integer('active')->default(1);
            //Crea los atributos Create at y modified at          
            $table->timestamps();
        });
                   //Con esto inserto un articulo por default al correr la migracion
            DB::table('product')
                        ->insert(array(
                                'cod'=>'MODHP12A',
                                'name'=>'Modulo HP 12A', 
                                'description' => 'Modulo para impresoras HP',  
                                'idncategorie' => '1',  
                                'cost' => '1500',                                                                  
                                'price1' => '1700',
                                'price2' => '1900',
                                'price3' => '2100'                                
                               ));

                     

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Esta funcion permite eliminar la tabla al ahcer roolback (ir hacia atras)
        Schema::drop('product');
    }
}
